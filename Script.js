
window.onload = function(){

    //Variables
    var audio = document.getElementById("audio");
    audio.volume = document.getElementById("volume").value;

    //Eventos
    //Comentario
    document.getElementById("reproducir").onclick = function(){audio.play()}
    document.getElementById("pausar").onclick = function(){audio.pause()}
    document.getElementById("parar").onclick = function(){audio.load()}
    document.getElementById("volume").onchange = function(e){audio.volume = e.target.value}
    
    audio.ontimeupdate = function(){
        document.getElementById("time").max = audio.duration;
        document.getElementById("time").value = audio.currentTime;
    }

    //Informacion de fichero
    var rutaAbsoluta = audio.currentSrc;  
    var posicionUltimaBarra = rutaAbsoluta.lastIndexOf("/");
    var rutaRelativa = rutaAbsoluta.substring( posicionUltimaBarra + "/".length , rutaAbsoluta.length );
    document.getElementById("cancion").innerHTML = rutaRelativa;
}


var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
// crea un oscilador
var oscilador  = audioCtx.createOscillator();
// establece la frecuencia de oscilación en hertzios
oscilador.frequency.value = 400;
// establece el tipo de oscilador
// puede tomar uno de estos valores: "sine", "square", "sawtooth", "triangle" y "custom".
// el valor por defecto es "sine".
oscilador.type = "sawtooth";
// conecta el oscilador con con su destino ( altavoces, auriculares . . . etc )
oscilador.connect(audioCtx.destination);

function Iniciar(){
  // inicia el oscilador
  oscilador.start(audioCtx.currentTime);
  // para el oscilator despues de 3 segundos
  oscilador.stop(audioCtx.currentTime + 1);
}



